#!/usr/bin/env bash

[ -d public/ ] && rm -rf public/ && echo "Deleted existing public/ directory"

hugo && rsync -avz --delete public/ ${SUMMIT_HOST}:/var/www/stopcopcitysummit.com && ssh ${SUMMIT_HOST} "chown -R www-data:www-data /var/www/stopcopcitysummit.com"
