---
title: 'Donations'
description: How to donate money
longTitle: 'Donations'
---

### I have money, and I want to give it to the summit!

We can accept money through [PayPal](https://www.paypal.com/donate/?hosted_button_id=UARVDS9VSCY3S), [CashApp](https://cash.app/$TucsonWDS), or [Venmo](https://venmo.com/u/wdstucson)

&nbsp;

Feel free to send these links to your old friends, your mom, and strangers you meet on the road to Tucson. Maybe offer a ride and bring them all to the summit with you, too!