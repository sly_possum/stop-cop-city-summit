---
title: 'Info'
description: When, where, and how to get places
longTitle: 'Info'
---

{{< accordion title="## + Lodging" >}}
There will be a large protest camp in the city where all who can are encouraged to stay. The more people stay at camp, the more fun we have and the safer it is from cops. If you can’t join the camp, feel free to find your own housing, or fill out [this form](https://form.jotform.com/240074858715058) to request a free place to stay in Tucson. Space will be limited, but local hosts will do their best to accommodate as they are able. If you are a local who would like to offer a space, please [click here](https://form.jotform.com/240077262335048).

{{< accordion title="### + Camp" class="nested-accordion" >}}
The camp will last the entire length of the summit and is open to the public. All campers are expected to respect the land, the neighborhood and each other. Please come prepared to be self-sufficient with rain-proof tents, sleeping bags and mats, and whatever else you might need. Communal camping gear will be shared whenever available, but none is guaranteed. If you have something to share, please bring it. 

&nbsp;

To the TPD officer reading this: attempting to evict this protest camp will result in many serious headaches including, but not limited to a terrible day with the press. Leave the camp alone and it will be gone before you know it ;)
{{< /accordion >}}

{{< accordion title="### + Weather" class="nested-accordion" >}}
Tucson weather can vary drastically, even over the course of one day. Be prepared for daytime temps as high as 80°F (and sunny) and nights as low as 25°F (and windy). Rain is possible. Check the forecast as the dates approach.
{{< /accordion >}}

{{< accordion title="### + Food" class="nested-accordion" no-bottom-bar="true" >}}
There will be a kitchen set up at camp to cook free communal meals for all summit attendees. Meals will happen 2-3 times per day, with vegan and gluten free options available. Let the cooks know if you have other dietary restrictions they can accommodate. 

&nbsp;

The kitchen will be entirely volunteer run and depends upon donated food, money, and work. Please plan on contributing at least a few hours of work to the kitchen (cooking, cleaning, moving supplies) and donate money at the kitchen if you can. Any food donations are welcome, especially ready-to-eat snacks, toppings/sauces, bread, fruits and veggies.
{{< /accordion >}}
{{< /accordion >}}

{{< accordion title="## + Travel" >}} 

Carpooling with a crew from your town is always the best option!

{{< accordion title="### + Flying" class="nested-accordion" >}}
TUS airport is most convenient, but can be expensive. Public transit is free from the airport. PHX airport is usually cheaper with more flight options.

&nbsp;

Getting from PHX Airport to Tucson:

&nbsp;

Take the SkyTrain to the 44th Street Station.
  - Flix Bus (Take the University stop in Tucson)
  - [Groome Transportation shuttle](https://groometransportation.com/) (Take the University stop in Tucson)
  - Greyhound (From Phoenix Greyhound Station to Tucson Greyhound Station)
  - Some dedicated shuttles might be available from PHX. Check back here and [@stopcopcitysummit](https://instagram.com/stopcopcitysummit) on Instagram for updates
{{< /accordion >}}

{{< accordion title="### + Getting Around Tucson" class="nested-accordion" no-bottom-bar="true" >}}
  - **Buses** are free, but not the most reliable. [Website for routes and schedules](https://www.suntran.com/).
  - **Bicycles** are great options in Tucson. BICAS is a bike cooperative that has [bikes for rent](https://bicas.org/bikes/rent-a-bicycle/). The city has [bike share bikes](https://tugobikeshare.com/) too.
  - **Shuttles** might be available for specific events/locations. Please check back here and @stopcopcitysummit on Instagram for updates.
{{< /accordion >}}
{{< /accordion >}}


Check back often and follow [@stopcopcitysummit](https://instagram.com/stopcopcitysummit) on Instagram for updates.
This page will continue to be updated with more information as the event approaches.