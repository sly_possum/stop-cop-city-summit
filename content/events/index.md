---
title: 'Events'
description: What's going on?
longTitle: 'Events'
---

All events are organized autonomously. Anyone is welcome to host events as part of the summit and add them to this calendar. Anyone is welcome to attend any of these events unless otherwise noted in the event description.

&nbsp;

{{< accordion title="### + Venue Info (Updated!)" >}}
Most events will be outside at camp or in a nearby park. The updated map below shows all locations for Camp along with Park Zones A through D.

&nbsp;

<img src="/map.jpeg" style="width: auto; max-width: 100%;"/>

- <span style="color:#00855b; font-weight: bold">Camp Living Room</span> (Close to meals and campers, uneven ground, no seating, noisy, some tarp cover for rain and shade. max ~50 people)
- <span style="color:#ce1212; font-weight: bold">Park Zone A</span> (Shady, two picnic tables, wheelchair accessible, no roof, some road noise)
- <span style="color:#f6c811; font-weight: bold">Park Zone B</span> (Shady, one picnic table, wheelchair accessible, no roof)
- <span style="color:#7742a9; font-weight: bold">Park Zone C</span> (Full sun, large open field, good for games and large groups)
- <span style="color:#2951b9; font-weight: bold">Park Zone D</span> (Under a pavilion/roof, a few picnic tables, paved & wheelchair accessible. Max ~50 people under the roof)
- <span style="color:#f16c20; font-weight: bold">Other</span> (Check the event description or contact event hosts for details)
{{< /accordion >}}

{{< accordion title="### + Want to host an event?" >}}
Go for it! You can add yours directly to the calendar here and make edits for roughly 30 minutes after adding. If you need to make changes after closing the page or after the timer expires, please email [nationwidesummitarizona@riseup.net](mailto:nationwidesummitarizona@riseup.net) with the modifications you’d like to make. 

&nbsp;

As an event host, you are responsible for your own supplies, promotion, and facilitation. Consider including contact info in your event description in case anyone has questions. Please include info about the accessibility of your venue whenever possible.

&nbsp;

If you need an indoor venue or other accommodations, email [nationwidesummitarizona@riseup.net](mailto:nationwidesummitarizona@riseup.net), and they will do their best to help connect you with a venue (no guarantees!)
{{< /accordion >}}

&nbsp;

<!-- allows for target="_blank" -->
Click <a href="https://teamup.com/ksm6yaksfhqjh11nd5" target="_blank">here</a> to view the calendar in its own window!

&nbsp;

<iframe id="desktop-calendar" src="https://teamup.com/ksm6yaksfhqjh11nd5?showProfileAndInfo=0&showSidepanel=1&disableSidepanel=1&showViewSelector=1&showMenu=0&showAgendaHeader=1&showAgendaDetails=0&showYearViewHeader=1" style="width: 115%; height: 800px; border: 2px solid #cccccc" loading="lazy" frameborder="0"></iframe>

<iframe id="mobile-calendar" src="https://teamup.com/ksm6yaksfhqjh11nd5?view=d&showProfileAndInfo=0&showSidepanel=1&disableSidepanel=1&showViewSelector=1&showMenu=0&showAgendaHeader=1&showAgendaDetails=0&showYearViewHeader=1" style="width: 115%; height: 800px; border: 2px solid #cccccc" loading="lazy" frameborder="0"></iframe>
