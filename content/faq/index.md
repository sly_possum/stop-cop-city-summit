---
title: 'FAQ'
description: Frequently Asked Questions
longTitle: 'Frequently Asked Questions'
---

{{< accordion title="### + Where do I go when I get to Tucson?" >}}

If you arrive on Friday afternoon, come to the Kickoff Cookout at The Park (2000 N Stone Ave) any time between 2pm and 5pm. At 5pm, we will all stroll to the camp site together, so bring your camping gear and try to make it by 5!

&nbsp;

At 5:30pm on Friday, the camp and park locations will be made public. Check back here if you arrive after that time.

{{< /accordion >}}

{{< accordion title="### + What is the summit?" >}}

Cop City, the $90M police training facility slated to be built in south Atlanta, would drastically increase police militarization and the spread of violent policing tactics across the world, from Atlanta to the US borderlands to Palestine. The contractors of Cop City are not bound by the geographic limits of one city or state. The movement to Stop Cop City cannot be, either.

&nbsp;

From February 23 – 26th, hundreds of people across the country will converge in Tucson for four days of protests, panels, workshops, live music, and strategy discussions.

{{< /accordion >}}


{{< accordion title="### + Why is the summit not in Atlanta?" >}}

The contractors, funders, insurance providers, and politicians intent on seeing Cop City through to its completion do not confine themselves to one particular city or state. The movement to Stop Cop City and Defend the Atlanta Forest cannot be limited by the boundaries of the Weelaunee Forest or the city limits of Atlanta, either. With unprecedented political repression in Atlanta--the murder of Tortuguita in January 2023, and more than 100 forest defenders facing serious charges--it is more important than ever that the movement to defend the forest resonate and spread across the country.

{{< /accordion >}}

{{< accordion title="### + Why is the summit in Tucson, Arizona?" >}}

Everyday people can fight Cop City anywhere there are contractors, funders, insurance providers, or politicians with ties to the project. Thousands of people in the Southwest and along the West Coast support the movement, yet mobilizations in Atlanta can be logistically difficult for them to attend. The Stop Cop City Summit in Tucson invites greater participation from these regions, which are rife with financial and political ties to Cop City. You can read more about connections to Cop City in the Southwest, and where you live, at [stopcopcitysolidarity.org](https://web.archive.org/web/20240129194801/https://www.stopcopcitysolidarity.org/).

{{< /accordion >}}

{{< accordion title="### + I don't want to risk arrest or serious charges, like domestic terrorism. Can I still participate?" >}}

The most intense repression that the movement has faced is particular to Georgia state law and the political terrain in Atlanta. Millions of dollars and unprecedented numbers of police officers have been mobilized to securitize and police the Weelaunee Forest. 42 people have indeed been charged (but not indicted) under the State of Georgia Domestic Terrorism law, and 61 people have been charged under the Georgia RICO Act. It is important to note that these are particular to Georgia state law and are not federal charges. There will be opportunities to learn more about the political terrain specific to Tucson early on in the summit.

&nbsp;

We cannot make guarantees regarding the safety of participation in any protest movement, including support roles and infrastructure. We simply know that--amidst the rise of far-right governments across the world, rampant environmental degradation, and the increasing millitarization of police forces--the risk of inaction is too great.

{{< /accordion >}}

{{< accordion title="### + What is the relationship between the Stop Cop City / Defend the Atlanta Forest movement and the genocide in Palestine?" >}}

Since 1992, the Georgia International Law Enforcement Exchange (GILEE) has trained law enforcement agencies across the world in political repression, crowd control, and munitions. This includes the Israeli Occupying Forces, who have murdered more than 30,000 Palestinians since October 7, 2023. If Cop City is built, repressive forces such as the IDF will be better trained to suppress movements for Palestinian liberation and slay Palestinian people. Cop City's direct link to the genocide in Palestine is indicative of the international stakes of the movement to Stop Cop City and Defend the Atlanta Forest. The Southwest is home to many corporations supplying arms to the Israeli military.

{{< /accordion >}}

{{< accordion title="### + What will the summit entail?" >}}

The Nationwide Summit to Stop Cop City will become what participants make of it, since events are self-organized and submitted by participants! There are already plans for a kick-off cookout and rave, as well as an assortment of workshops, panels, and protests. Similar mobilizations, such as the Weeks of Action in Atlanta, have included protests and demonstrations, live music, raves, talks and panels, film screenings, children's marches and youth activities, care clinics, skillshares, and more.

{{< /accordion >}}

{{< accordion title="### + What can I do between now and February 23 to prepare for the summit?" >}}

**Organize a group to travel to Tucson** 
&nbsp;

Though you can come to Tucson solo, it's more fun and cost-efficient to travel with friends! Reach out to friends who may want to attend the summit and begin coordinating travel and lodging with them.

&nbsp;

**Spread the word about the summit** 
&nbsp;

Informational events, fundraisers, benefit shows, protests, and film screenings are great ways to share information about the summit in Tucson and meet people who want to attend.

&nbsp;

**Fundraise** 
&nbsp;

Hosting shows, raffles, or other fundraising events can alleviate the travel costs associated with coming to the Summit. These take time to plan, so it's best to start now!

&nbsp;

**Organize an event at the summit** 
&nbsp;

Add your event by checking out the [events page](/events)

{{< /accordion >}}

{{< accordion title="### + Will the summit be accessible?" >}}

Summit events are autonomously organized by participants and will take place in a variety of venues and contexts that may or may not meet each person's accessibility needs. To learn more about a particular event or request accommodations, please contact the event's organizers.

{{< /accordion >}}

{{< accordion title="### + How do I submit an event for the Stop Cop City Summit?" >}}

Add your event by checking out the [events page](/events)

{{< /accordion >}}
